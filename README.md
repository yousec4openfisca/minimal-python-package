# Packge Sample

## Installation

```
git clone https://gitlab.com/yousec4openfisca/package-python
```

```
pip install -e package-python/
```

## Tests

```
cd package-python/
behave tests
```
