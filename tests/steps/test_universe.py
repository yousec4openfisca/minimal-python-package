from behave import given, when, then
from universe import universe

@given(u'Question')
def step_impl(context):
	context.answer = universe.answer()
	pass

@when(u'Waiting for an answer')
def step_impl(context):
	pass

@then(u'Give answer')
def step_impl(context):
	assert context.answer == 42